plugins {
    kotlin("plugin.serialization") version "1.8.10"
    `kotlin-dsl`
    `maven-publish`
}

val projectName: String by project
val projectVersion: String by project
val projectGroup: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

kotlin.jvmToolchain(8)

repositories {
    /*mavenLocal()*/
    mavenCentral()
    /*maven("https://repo.weavemc.dev/releases")*/
    maven("https://gitlab.com/api/v4/projects/64882766/packages/maven") // Weave Internals
}

dependencies {
    // OW2 ASM
    implementation(libs.asm)
    implementation(libs.asmCommons)

    // Kotlinx.serialization JSON library
    implementation(libs.kxSerJSON)
    implementation(libs.mappingsUtil)
    implementation(libs.weaveInternals)
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompilationTask<*>>()
    .configureEach {
        compilerOptions
            .languageVersion
            .set(org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_1_9)
    }

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

gradlePlugin {
    plugins {
        create("weave") {
            id = projectGroup
            displayName = projectName
            description =
                "Implements Remapped Minecraft libraries intended for developing Minecraft mods with Weave"
            implementationClass = "$projectGroup.WeaveGradle"
        }
    }
}
